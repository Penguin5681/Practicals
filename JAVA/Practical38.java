import java.applet.Applet;
import java.awt.Graphics;

public class Practical38 extends Applet {
    @Override
    public void paint(Graphics g) {
        g.drawString("Hello, this is an applet!", 50, 50);
    }
}
